import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
//import { createVuePlugin } from "vite-plugin-vue2";

// https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [vue()]
// })

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: [
      {
        find: "@",
        replacement: path.resolve(__dirname, "src"),
      },
    ],
  },
});
